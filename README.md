# testeCiag

#### Para abrir o arquivo formato ipnynb é necessário instalar em sua maquina a ferramenta Jupyter-notebook ou acessar através do google colab(Basta importar os arquivos na ferramenta). 

#### Insigths Obtidos através da analise de dados do COVID: https://brasil.io/dataset/covid19/caso_full/

### * Calcular total de informações dos respectivos Estados
        -Contagem total de pessoas testadas para covid e confirmadas por Estado.
        -Contagem total de pessoas mortas por Estado
        -Contagem total de pessoas confirmadas para covid que sobreviveram em seu respectivo Estado
        -Porcentagem de sobrevivência de pessoas que foram positivasdas para covid em seu respectivo Estado

### * Calcular total de informações do Brasil
        -Total de casos de Covid confirmados no Brasil: 53400630
 
        -Total de Mortos no Brasil: 1274532
 
        -Total de casos positivos que sobreviveram no Brasil: 52126098

### * Regressão linear para analisar a quantidade de mortos proporcionalmente a estimativa populacional


